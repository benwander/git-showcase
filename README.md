<h1>How to Create a Git Project Like This One</h1>
The following steps will show you how to create a local git project and link it to a git.cs.du.edu remote repository.<br/><br/>

**CREATE A LOCAL GIT PROJECT**
1. Create a local project
2. Add a .gitignore file at the root directory <br/>
    a. .gitignore for Unity: https://github.com/github/gitignore/blob/main/Unity.gitignore <br/>
    b. .gitignore for Unreal: https://github.com/github/gitignore/blob/main/UnrealEngine.gitignore

**CREATE A REMOTE GIT PROJECT**
1. Go to https://git.cs.du.edu/
2. Click the New Project button
3. Click Create Blank Project
4. Name your project, change the Visibility to Public, and uncheck Initialize Repository With Read Me -- then click Create Project

**CREATE A LOCAL REPOSITORY AND LINK WITH REMOTE PROJECT**
1. On your local computer, open a terminal window at the root directory of your local project
2. Run the following commands: <br/>
    a. git init --initial-branch=main <br/>
    b. git remote add origin git@git.cs.du.edu:[YOURUSERNAME]/[your-project-name].git <br/>
    c. git add . <br/>
    d. git commit -m "Initial commit" <br/>
    e. git push --set-upstream origin main <br/>

**ADD MEMBERS TO YOUR GIT PROJECT**
1. Go to https://git.cs.du.edu/
2. Click on your project
3. On the left panel, click Manage > Members
4. On the top right, click Invite Members
5. Invite anyone on your team by their username as a Developer (their name usually works in the search field)
6. Invite me (@benwander) as a Developer
